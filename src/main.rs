use {
    ndarray::Array2,
    rand::{seq::SliceRandom, Rng},
    std::collections::VecDeque,
    tetra::{
        graphics::{self, Color, Texture},
        input::{self, Key},
        math::Vec2,
        time::{self, Timestep},
        Context, ContextBuilder, State,
    },
};

const BLOCK_SIZE: u32 = 16;

const BORDER_SIZE: u32 = 1;

const MARGIN_RIGHT: u32 = 8;

const WINDOW_WIDTH: u32 = (FIELD_WIDTH + MARGIN_RIGHT + 2 * BORDER_SIZE) * BLOCK_SIZE;
const WINDOW_HEIGHT: u32 = (FIELD_HEIGHT + 2 * BORDER_SIZE) * BLOCK_SIZE;

const FIELD_WIDTH: u32 = 10;
const FIELD_HEIGHT: u32 = 20;

const PIECE_PREVIEW_X: i32 = 1;
const PIECE_PREVIEW_Y: i32 = 1;

const HELD_PREVIEW_X: i32 = 1;
const HELD_PREVIEW_Y: i32 = 8;

const PIECE_DEFS: [PieceDef; 7] = [
    // I
    PieceDef {
        block: Block::Cyan,
        filled: [
            [false, false, true, false],
            [false, false, true, false],
            [false, false, true, false],
            [false, false, true, false],
        ],
    },
    // flipped L
    PieceDef {
        block: Block::Blue,
        filled: [
            [false, true, true, false],
            [false, true, false, false],
            [false, true, false, false],
            [false, false, false, false],
        ],
    },
    // L
    PieceDef {
        block: Block::Orange,
        filled: [
            [false, false, false, false],
            [false, true, false, false],
            [false, true, false, false],
            [false, true, true, false],
        ],
    },
    // Square
    PieceDef {
        block: Block::Dee0,
        filled: [
            [false, false, false, false],
            [false, true, true, false],
            [false, true, true, false],
            [false, false, false, false],
        ],
    },
    // S
    PieceDef {
        block: Block::Green,
        filled: [
            [false, false, false, false],
            [false, false, true, true],
            [false, true, true, false],
            [false, false, false, false],
        ],
    },
    // T
    PieceDef {
        block: Block::Purple,
        filled: [
            [false, false, false, false],
            [false, false, true, false],
            [false, true, true, true],
            [false, false, false, false],
        ],
    },
    // Z
    PieceDef {
        block: Block::Red,
        filled: [
            [false, false, false, false],
            [false, true, true, false],
            [false, false, true, true],
            [false, false, false, false],
        ],
    },
];

#[derive(Debug, Clone)]
pub struct PieceDef {
    pub block: Block,
    pub filled: [[bool; 4]; 4],
}

impl PieceDef {
    pub fn block_at(&self, x: i32, y: i32) -> Option<Block> {
        if self.filled[y as usize][x as usize] {
            match self.block {
                Block::Dee0 | Block::Dee1 | Block::Dee2 | Block::Dee3 => Some(if y < 2 {
                    if x < 2 {
                        Block::Dee0
                    } else {
                        Block::Dee1
                    }
                } else {
                    if x < 2 {
                        Block::Dee3
                    } else {
                        Block::Dee2
                    }
                }),
                other => Some(other),
            }
        } else {
            None
        }
    }
}

#[derive(Debug, Clone, Copy)]
#[repr(u8)]
pub enum Block {
    Cyan = 0,
    Blue = 1,
    Orange = 2,
    Yellow = 3,
    Green = 4,
    Purple = 5,
    Red = 6,
    Dee0 = 7,
    Dee1 = 8,
    Dee2 = 9,
    Dee3 = 10,
}

impl Block {
    pub fn has_face(self) -> bool {
        match self {
            Block::Dee0 => false,
            Block::Dee1 => false,
            Block::Dee2 => false,
            Block::Dee3 => false,
            _ => true,
        }
    }
}

#[derive(Debug, Clone)]
pub struct Piece {
    pub x: i32,
    pub y: i32,
    pub face_id: usize,
    pub filled: [[Option<Block>; 4]; 4],
}

impl Piece {
    pub fn from_piece_def(x: i32, y: i32, piece_def: &PieceDef, face_id: usize) -> Piece {
        let mut filled = [[None; 4]; 4];
        for iy in 0..4 {
            for ix in 0..4 {
                filled[iy as usize][ix as usize] = piece_def.block_at(ix, iy);
            }
        }
        Piece {
            x,
            y,
            filled,
            face_id,
        }
    }
}

#[derive(Debug, Clone)]
pub struct Game {
    pub tick: usize,
    pub next_pieces: VecDeque<u8>,
    pub field: Field,
    pub current_piece: Option<Piece>,
    pub held_piece: Option<Piece>,
    pub block_textures: [Texture; 11],
    pub block_faces: [Texture; 3],
    pub border_texture: Texture,
}

impl Game {
    pub fn new(ctx: &mut Context, width: u32, height: u32) -> tetra::Result<Game> {
        let tick = 0;
        let field = Field::new(width, height);
        let block_textures = [
            Texture::new(ctx, "resources/block-cyan.png")?,
            Texture::new(ctx, "resources/block-blue.png")?,
            Texture::new(ctx, "resources/block-orange.png")?,
            Texture::new(ctx, "resources/block-yellow.png")?,
            Texture::new(ctx, "resources/block-green.png")?,
            Texture::new(ctx, "resources/block-purple.png")?,
            Texture::new(ctx, "resources/block-red.png")?,
            Texture::new(ctx, "resources/block-dee-0.png")?,
            Texture::new(ctx, "resources/block-dee-1.png")?,
            Texture::new(ctx, "resources/block-dee-2.png")?,
            Texture::new(ctx, "resources/block-dee-3.png")?,
        ];
        let block_faces = [
            Texture::new(ctx, "resources/face-0.png")?,
            Texture::new(ctx, "resources/face-1.png")?,
            Texture::new(ctx, "resources/face-2.png")?,
        ];
        let border_texture = Texture::new(ctx, "resources/border.png")?;
        let next_pieces = VecDeque::new();
        let current_piece = None;
        let held_piece = None;
        Ok(Game {
            tick,
            field,
            next_pieces,
            current_piece,
            held_piece,
            block_textures,
            block_faces,
            border_texture,
        })
    }

    pub fn step_bag(&mut self) {
        let mut rng = rand::thread_rng();
        if self.next_pieces.is_empty() {
            let mut new_pieces = [0, 1, 2, 3, 4, 5, 6];
            new_pieces.shuffle(&mut rng);
            for i in 0..7 {
                self.next_pieces.push_back(new_pieces[i]);
            }
        }
        if self.current_piece.is_none() {
            let piece_id = self.next_pieces.pop_front().unwrap(); // never panics, due to the code before this
            self.current_piece = Some(Piece::from_piece_def(
                FIELD_WIDTH as i32 / 2,
                0,
                &PIECE_DEFS[piece_id as usize],
                rng.gen_range(0, self.block_faces.len()),
            ));
        }
    }

    pub fn step_gravity(&mut self) {
        if let Some(piece) = &mut self.current_piece {
            if self.field.piece_can_fall(piece) {
                let mut lost = false;
                'outer_b: for iy in 0..4 {
                    for ix in 0..4 {
                        if piece.filled[iy as usize][ix as usize].is_some() {
                            let field_x = (piece.x + ix - 2) as i32;
                            let field_y = (piece.y + iy - 2) as i32;
                            // only y>=0 here is required as a bounds check, because the rest
                            // are guaranteed to be in bounds
                            if field_y >= 0 {
                                self.field.data[(field_x as usize, field_y as usize)] = piece
                                    .filled[iy as usize][ix as usize]
                                    .map(|block| (block, piece.face_id));
                            } else {
                                lost = true;
                                break 'outer_b;
                            }
                        }
                    }
                }
                if lost {
                    panic!("lost the game");
                } else {
                    self.current_piece = None;
                    for iy in 0..FIELD_HEIGHT {
                        let mut gaps = false;
                        'outer_c: for ix in 0..FIELD_WIDTH {
                            if self.field.data[(ix as usize, iy as usize)].is_none() {
                                gaps = true;
                                break 'outer_c;
                            }
                        }
                        if !gaps {
                            for iiy in (1..=iy).rev() {
                                for ix in 0..FIELD_WIDTH {
                                    self.field.data[(ix as usize, iiy as usize)] =
                                        self.field.data[(ix as usize, iiy as usize - 1)];
                                }
                            }
                            for ix in 0..FIELD_WIDTH {
                                self.field.data[(ix as usize, 0)] = None;
                            }
                        }
                    }
                }
            } else {
                piece.y += 1;
            }
        }
    }
}

#[derive(Debug, Clone)]
pub struct Field {
    pub width: u32,
    pub height: u32,
    pub data: Array2<Option<(Block, usize)>>,
}

impl Field {
    pub fn new(width: u32, height: u32) -> Field {
        let data = Array2::default((width as usize, height as usize));
        Field {
            width,
            height,
            data,
        }
    }

    pub fn piece_collides(&self, piece: &Piece) -> bool {
        for iy in 0..4 {
            for ix in 0..4 {
                if piece.filled[iy as usize][ix as usize].is_some() {
                    let field_x = (piece.x + ix - 2) as i32;
                    let field_y = (piece.y + iy - 2) as i32;
                    if field_x < 0
                        || field_x >= self.width as i32
                        || field_y < 0
                        || field_y >= self.height as i32
                    {
                        return true;
                    } else {
                        if self.data[(field_x as usize, field_y as usize)].is_some() {
                            return true;
                        }
                    }
                }
            }
        }
        false
    }

    pub fn piece_can_fall(&self, piece: &Piece) -> bool {
        for iy in 0..4 {
            for ix in 0..4 {
                if piece.filled[iy as usize][ix as usize].is_some() {
                    let field_x = (piece.x + ix - 2) as i32;
                    let field_y = (piece.y + iy - 2 + 1) as i32;
                    if field_y >= self.height as i32 {
                        return true;
                    } else if field_x >= 0 && field_x < self.width as i32 && field_y >= 0 {
                        if self.data[(field_x as usize, field_y as usize)].is_some() {
                            return true;
                        }
                    }
                }
            }
        }
        false
    }
}

impl State for Game {
    fn update(&mut self, ctx: &mut Context) -> tetra::Result<()> {
        for key in input::get_keys_pressed(ctx) {
            match key {
                Key::Left => {
                    if let Some(piece) = &mut self.current_piece {
                        piece.x -= 1;
                        if self.field.piece_collides(&piece) {
                            piece.x += 1;
                        }
                    }
                }
                Key::Right => {
                    if let Some(piece) = &mut self.current_piece {
                        piece.x += 1;
                        if self.field.piece_collides(&piece) {
                            piece.x -= 1;
                        }
                    }
                }
                Key::Up => {
                    if let Some(piece) = &mut self.current_piece {
                        let mut new_filled = [[None; 4]; 4];
                        for iy in 0..4 {
                            for ix in 0..4 {
                                new_filled[ix][iy] = piece.filled[3 - iy][ix];
                            }
                        }
                        let new_piece = Piece {
                            filled: new_filled,
                            ..*piece
                        };
                        if !self.field.piece_collides(&new_piece) {
                            *piece = new_piece;
                        }
                    }
                }
                Key::LeftShift => {
                    if let Some(piece) = self.current_piece.take() {
                        if let Some(mut held_piece) = self.held_piece.take() {
                            held_piece.x = piece.x;
                            held_piece.y = piece.y;
                            self.current_piece = Some(held_piece);
                        }
                        self.held_piece = Some(piece);
                    }
                }
                _ => (),
            }
        }
        self.step_bag();
        if self.tick % 70 == 0 || input::is_key_down(ctx, Key::Down) {
            self.step_gravity();
        }
        self.tick += 1;
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> tetra::Result<()> {
        graphics::clear(ctx, Color::rgb(0., 0., 0.));
        if let Some(piece) = &self.current_piece {
            for iy in 0..4 {
                for ix in 0..4 {
                    if let Some(block) = piece.filled[iy as usize][ix as usize] {
                        let x = (piece.x + ix - 2 + MARGIN_RIGHT as i32 + BORDER_SIZE as i32)
                            * BLOCK_SIZE as i32;
                        let y = (piece.y + iy - 2 + BORDER_SIZE as i32) * BLOCK_SIZE as i32;
                        graphics::draw(
                            ctx,
                            &self.block_textures[block as usize],
                            Vec2::new(x as f32, y as f32),
                        );
                        if block.has_face() {
                            graphics::draw(
                                ctx,
                                &self.block_faces[piece.face_id],
                                Vec2::new(x as f32, y as f32),
                            );
                        }
                    }
                }
            }
        }
        for ((ix, iy), value) in self.field.data.indexed_iter() {
            let x = (MARGIN_RIGHT + BORDER_SIZE + ix as u32) as i32 * BLOCK_SIZE as i32;
            let y = (BORDER_SIZE + iy as u32) as i32 * BLOCK_SIZE as i32;
            if let Some((block, face_id)) = *value {
                graphics::draw(
                    ctx,
                    &self.block_textures[block as usize],
                    Vec2::new(x as f32, y as f32),
                );
                if block.has_face() {
                    graphics::draw(
                        ctx,
                        &self.block_faces[face_id],
                        Vec2::new(x as f32, y as f32),
                    );
                }
            }
        }
        if let Some(&piece_def_id) = self.next_pieces.front() {
            let piece_def = &PIECE_DEFS[piece_def_id as usize];
            for iy in 0..4 {
                for ix in 0..4 {
                    if let Some(block) = piece_def.block_at(ix, iy) {
                        let x = (ix + PIECE_PREVIEW_X + BORDER_SIZE as i32) * BLOCK_SIZE as i32;
                        let y = (iy + PIECE_PREVIEW_Y + BORDER_SIZE as i32) * BLOCK_SIZE as i32;
                        graphics::draw(
                            ctx,
                            &self.block_textures[block as usize],
                            Vec2::new(x as f32, y as f32),
                        );
                    }
                }
            }
        }
        if let Some(held_piece) = &self.held_piece {
            for iy in 0..4 {
                for ix in 0..4 {
                    if let Some(block) = held_piece.filled[iy as usize][ix as usize] {
                        let x = (ix + HELD_PREVIEW_X + BORDER_SIZE as i32) * BLOCK_SIZE as i32;
                        let y = (iy + HELD_PREVIEW_Y + BORDER_SIZE as i32) * BLOCK_SIZE as i32;
                        graphics::draw(
                            ctx,
                            &self.block_textures[block as usize],
                            Vec2::new(x as f32, y as f32),
                        );
                    }
                }
            }
        }
        for iy in 0..FIELD_HEIGHT + 2 {
            for ix in 0..FIELD_WIDTH + 2 + MARGIN_RIGHT {
                if (ix <= MARGIN_RIGHT
                    || iy == 0
                    || ix == FIELD_WIDTH + MARGIN_RIGHT + 1
                    || iy == FIELD_HEIGHT + 1)
                    && !(ix >= 1 && iy >= 1 && ix <= 6 && iy <= 6)
                    && !(ix >= 1 && iy >= 8 && ix <= 6 && iy <= 13)
                {
                    let x = ix * BLOCK_SIZE;
                    let y = iy * BLOCK_SIZE;
                    graphics::draw(ctx, &self.border_texture, Vec2::new(x as f32, y as f32));
                }
            }
        }
        Ok(())
    }
}

fn main() -> tetra::Result<()> {
    ContextBuilder::new("Tetris", WINDOW_WIDTH as i32, WINDOW_HEIGHT as i32)
        .build()?
        .run(|ctx| {
            time::set_timestep(ctx, Timestep::Fixed(60.));
            Game::new(ctx, FIELD_WIDTH, FIELD_HEIGHT)
        })
}
