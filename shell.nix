{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  buildInputs = with pkgs; [
    freetype
    fontconfig
    cmake
    alsaLib
    pkg-config
    SDL2
  ];
}
